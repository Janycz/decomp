﻿using System.Globalization;
using System.IO;

namespace Decomp
{
    public static class Presentations
    {
        public static string DecompileFlags(int iFlag)
        {
            switch (iFlag)
            {
                case 3:
                    return "prsntf_read_only|prsntf_manual_end_only";
                case 2:
                    return "prsntf_manual_end_only";
                case 1:
                    return "prsntf_read_only";
                default:
                    return iFlag.ToString(CultureInfo.GetCultureInfo("en-US"));
            }
        }

        public static void Decompile()
        {
            var fPresentations = new Text(Common.InputPath + @"\presentations.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_presentations.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.PresentationsHeader);
            fPresentations.GetString();
            int iPresentations = fPresentations.GetInt();
            for (int i = 0; i < iPresentations; i++)
            {
                fSource.Write("  (\"{0}\"", fPresentations.GetWord());

                int iFlag = fPresentations.GetInt();
                fSource.Write(", {0}", DecompileFlags(iFlag));

                int iMesh = fPresentations.GetInt();
                if (iMesh >= 0 && iMesh < Common.Meshes.Length)
                    fSource.Write(", mesh_{0}", Common.Meshes[iMesh]);
                else
                    fSource.Write(", {0}", iMesh);
                fSource.Write(",\r\n  [\r\n");

                int iTriggers = fPresentations.GetInt();
                for (int t = 0; t < iTriggers; t++)
                {
                    double dInterval = fPresentations.GetDouble();
                    fSource.Write("    ({0},\r\n    [\r\n", Common.GetTriggerParam(dInterval));
                    int iRecords = fPresentations.GetInt();
                    if (iRecords != 0)
                    {
                        //memcpy(indention, "      ", 7);
                        Common.PrintStatement(ref fPresentations, ref fSource, iRecords, "      ");
                    }
                    fSource.Write("    ]),\r\n");
                }
                fSource.Write("  ]),\r\n\r\n");
            }
            fSource.Write("]");
            fSource.Close();
            fPresentations.Close();
        }
    }
}
