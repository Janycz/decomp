﻿using System;
using System.IO;
using DWORD64 = System.UInt64;
using DWORD = System.UInt32;
using WORD = System.UInt16;

namespace Decomp
{
    public static class PartyTemplates
    {
        public static string DecompileFlags(DWORD64 dwFlag)
        {
            string strFlag = "";
            var wIcon = (WORD)(dwFlag & 0xFF);
            var wCarriesGoods = (WORD)((dwFlag & 0x00FF000000000000) >> 48);
            var wCarriesGold = (WORD)((dwFlag & 0xFF00000000000000) >> 56);

            if (wIcon != 0) strFlag = "icon_" + Common.MapIcons[wIcon] + "|";
            if (wCarriesGoods != 0) strFlag += "carries_goods(" + wCarriesGoods + ")|";
            if (wCarriesGold != 0) strFlag += "carries_gold(" + wCarriesGold + ")|";

            string[] strFlags = { "pf_disabled", "pf_is_ship", "pf_is_static", "pf_label_medium", "pf_label_large",
			"pf_always_visible", "pf_default_behavior", "pf_auto_remove_in_town", "pf_quest_party", "pf_no_label", "pf_limit_members",
			"pf_hide_defenders", "pf_show_faction", "pf_dont_attack_civilians", "pf_civilian" };
            DWORD[] dwFlags = { 0x00000100, 0x00000200, 0x00000400, 0x00001000, 0x00002000, 0x00004000, 0x00010000,
			0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x02000000, 0x04000000 };
            for (int i = 0; i < (dwFlags.Length); i++)
            {
                if (((DWORD)dwFlag & dwFlags[i]) != 0)
                {
                    strFlag += strFlags[i] + "|";
                }
            }

            strFlag = strFlag == "" ? "0" : strFlag.Remove(strFlag.Length - 1, 1);

            return strFlag;
        }

        public static string DecompilePersonality(DWORD dwPersonality)
        {
            switch (dwPersonality)
            {
                case 0x89:
                    return "soldier_personality";
                case 0x07:
                    return "merchant_personality";
                case 0x0B:
                    return "escorted_merchant_personality";
                case 0x138:
                    return "bandit_personality";
                default:
                    string strPersonality = "";
                    if ((dwPersonality & 0x100) != 0)
                    {
                        strPersonality = "banditness|";
                    }
                    
                    var wCourage = (WORD)(dwPersonality & 0xF);
                    var wAggressiveness = (WORD)((dwPersonality & 0xF0) >> 4);

                    if (wCourage >= 4 && wCourage <= 15)
                    {
                        strPersonality += "courage_" + wCourage + "|";
                    }
                    if (wAggressiveness > 0 && wAggressiveness <= 15)
                    {
                        strPersonality += "aggressiveness_" + wAggressiveness + "|";
                    }

                    strPersonality = strPersonality == "" ? "0" : strPersonality.Remove(strPersonality.Length - 1, 1);

                    return strPersonality;
            }
        }

        public static void Decompile()
        {
            var fTemplates = new Text(Common.InputPath + @"\party_templates.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_party_templates.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.PartyTemplatesHeader);
            fTemplates.GetString();
            int iTemplates = fTemplates.GetInt();
            for (int i = 0; i < iTemplates; i++)
            {
                fSource.Write("  (\"{0}\", \"{1}\"", fTemplates.GetWord().Remove(0, 3), fTemplates.GetWord());

                DWORD64 dwFlag = fTemplates.GetUInt64();
                fSource.Write(", {0}, {1}", DecompileFlags(dwFlag), fTemplates.GetInt());

                int iFaction = fTemplates.GetInt();
                if (iFaction >= 0 && iFaction < Common.Factions.Length)
                    fSource.Write(", fac_{0}", Common.Factions[iFaction]);
                else
                    fSource.Write(", {0}", iFaction);

                DWORD dwPersonality = fTemplates.GetUInt();
                fSource.Write(", {0}, [", DecompilePersonality(dwPersonality));

                
                string strTroopList = "";
                for (int iStack = 0; iStack < 6; iStack++)
                {
                    int iTroop = fTemplates.GetInt();
                    if (-1 == iTroop)
                        continue;
                    int iMinTroops = fTemplates.GetInt();
                    int iMaxTroops = fTemplates.GetInt();
                    DWORD dwMemberFlag = fTemplates.GetDWord();
                    strTroopList += String.Format("(trp_{0}, {1}, {2}{3}),", Common.Troops[iTroop], iMinTroops, iMaxTroops, dwMemberFlag == 1 ? ", pmf_is_prisoner" : "");
                }
                if (strTroopList.Length > 0)
                    strTroopList = strTroopList.Remove(strTroopList.Length - 1, 1);
                fSource.WriteLine("{0}]),", strTroopList);
            }
            fSource.Write("]");
            fSource.Close();
            fTemplates.Close();
        }
    }
}
