﻿using System.IO;

namespace Decomp
{
    public static class Strings
    {
        public static void Decompile()
        {
            var fStrings = new Text(Common.InputPath + @"\strings.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_strings.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.StringsHeader);
            fStrings.GetString();
            int iStrings = fStrings.GetInt();

            for (int s = 0; s < iStrings; s++)
                fSource.WriteLine("  (\"{0}\", \"{1}\"),", fStrings.GetWord().Remove(0, 4), fStrings.GetWord().Replace('_', ' '));

            fSource.Write("]");
            fSource.Close();
            fStrings.Close();
        }
    }
}
