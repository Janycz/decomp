﻿using System;
using System.Globalization;
using System.IO;
using DWORD = System.UInt32;

namespace Decomp
{
    public static class Common
    {
        public static string ModuleConstantsText = @"from ID_animations.py import *
from ID_factions.py import *
from ID_info_pages.py import *
from ID_items.py import *
from ID_map_icons.py import *
from ID_menus.py import *
from ID_meshes.py import *
from ID_mission_templates.py import *
from ID_music.py import *
from ID_particle_systems.py import *
from ID_parties.py import *
from ID_party_templates.py import *
from ID_postfx_params.py import *
from ID_presentations.py import *
from ID_quests.py import *
from ID_scenes.py import *
from ID_scene_props.py import *
from ID_scripts.py import *
from ID_skills.py import *
from ID_sounds.py import *
from ID_strings.py import *
from ID_tableau_materials.py import *
from ID_troops.py import *";

        public static string[] Procedures;
        public static string[] QuickStrings;
        public static string[] Strings;
        public static string[] Items;
        public static string[] Troops;
        public static string[] Factions;
        public static string[] Quests;
        public static string[] PTemps;
        public static string[] Parties;
        public static string[] Menus;
        public static string[] Sounds;
        public static string[] Skills;
        public static string[] Meshes;
        public static string[] Variables;
        public static string[] DialogStates;
        public static string[] Scenes;
        public static string[] MissionTemplates;
        public static string[] ParticleSystems;
        public static string[] SceneProps;
        public static string[] MapIcons;
        public static string[] Presentations;
        public static string[] Tableaus;
        public static string[] Animations;
        public static string[] Music;
        public static string[] Skins;
        public static string[] Operations;
        public static string InputPath;
        public static string OutputPath;

        public static string GetParam(ulong lParam)
        {
            ulong lTag = (lParam & 0xFF00000000000000) >> 56;
            switch (lTag)
            {
                case 1:
                    var iReg = (int)lParam;
                    return "reg" + Convert.ToString(iReg);
                case 2:
                    var iVariable = (int)lParam;
                    if (iVariable < Variables.Length)
                        return "\"$" + Variables[iVariable] + "\"";
                    return "\"$g_var" + iVariable + "\"";
                case 3:
                    return "\"str_" + Strings[(int)lParam] + "\"";
                case 4:
                    return "\"itm_" + Items[(int)lParam] + "\"";
                case 5:
                    return "\"trp_" + Troops[(int)lParam] + "\"";
                case 6:
                    return "\"fac_" + Factions[(int)lParam] + "\"";
                case 7:
                    return "\"qst_" + Quests[(int)lParam] + "\"";
                case 8:
                    return "\"pt_" + PTemps[(int)lParam] + "\"";
                case 9:
                    return "\"p_" + Parties[(int)lParam] + "\"";
                case 10:
                    return "\"scn_" + Scenes[(int)lParam] + "\"";
                case 11:
                    return "\"mt_" + MissionTemplates[(int)lParam] + "\"";
                case 12:
                    return "\"mnu_" + Menus[(int)lParam] + "\"";
                case 13:
                    return "\"script_" + Procedures[(int)lParam] + "\"";
                case 14:
                    return "\"psys_" + ParticleSystems[(int)lParam] + "\"";
                case 15:
                    return "\"spr_" + SceneProps[(int)lParam] + "\"";
                case 16:
                    return "\"snd_" + Sounds[(int)lParam] + "\"";
                case 17:
                    return "\":var" + Convert.ToString((int)lParam) + "\"";
                case 18:
                    return "\"icon_" + MapIcons[(int)lParam] + "\"";
                case 19:
                    return "\"skl_" + Skills[(int)lParam] + "\"";
                case 20:
                    return "\"mesh_" + Meshes[(int)lParam] + "\"";
                case 21:
                    return "\"prsnt_" + Presentations[(int)lParam] + "\"";
                case 22:
                    return "\"@" + QuickStrings[(int)lParam] + "\"";
                case 23:
                    return "\"track_" + Music[(int)lParam] + "\"";
                case 24:
                    return "\"tableau_" + Tableaus[(int)lParam] + "\"";
                case 25:
                    return "\"anim_" + Animations[(int)lParam] + "\"";
                default:
                    return lParam.ToString(CultureInfo.GetCultureInfo("en-US"));
            }
        }

        public static string GetTriggerParam(double dblParam)
        {
        	switch ((int)dblParam)
            {
                case -2: return "ti_on_game_start";
                case -5: return "ti_simulate_battle";
                case -6: return "ti_on_party_encounter";
                case -8: return "ti_question_answered";
                case -15: return "ti_server_player_joined";
                case -16: return "ti_on_multiplayer_mission_end";
                case -19: return "ti_before_mission_start";
                case -20: return "ti_after_mission_start";
                case -21: return "ti_tab_pressed";
                case -22: return "ti_inventory_key_pressed";
                case -23: return "ti_escape_pressed";
                case -24: return "ti_battle_window_opened";
                case -25: return "ti_on_agent_spawn";
                case -26: return "ti_on_agent_killed_or_wounded";
                case -27: return "ti_on_agent_knocked_down";
                case -28: return "ti_on_agent_hit";
                case -29: return "ti_on_player_exit";
                case -30: return "ti_on_leave_area";
                case -40: return "ti_on_scene_prop_init";
                case -42: return "ti_on_scene_prop_hit";
                case -43: return "ti_on_scene_prop_destroy";
                case -44: return "ti_on_scene_prop_use";
                case -45: return "ti_on_scene_prop_is_animating";
                case -46: return "ti_on_scene_prop_animation_finished";
                case -47: return "ti_on_scene_prop_start_use";
                case -48: return "ti_on_scene_prop_cancel_use";
                case -50: return "ti_on_init_item";
                case -51: return "ti_on_weapon_attack";
                case -52: return "ti_on_missile_hit";
                case -53: return "ti_on_item_picked_up";
                case -54: return "ti_on_item_dropped";
                case -55: return "ti_on_agent_mount";
                case -56: return "ti_on_agent_dismount";
                case -57: return "ti_on_item_wielded";
                case -58: return "ti_on_item_unwielded";
                case -60: return "ti_on_presentation_load";
                case -61: return "ti_on_presentation_run";
                case -62: return "ti_on_presentation_event_state_change";
                case -63: return "ti_on_presentation_mouse_enter_leave";
                case -64: return "ti_on_presentation_mouse_press";
                case -70: return "ti_on_init_map_icon";
                case -71: return "ti_on_order_issued";
                case -100: return "ti_on_scene_prop_stepped_on";
                case -101: return "ti_on_init_missile";
                case -103: return "ti_on_shield_hit";
                case -104: return "ti_on_missile_dive";
                case -105: return "ti_on_agent_start_reloading";
                case -106: return "ti_on_agent_end_reloading";
                case 100000000: return "ti_once";
                default: return dblParam.ToString(CultureInfo.GetCultureInfo("en-US"));
            }
        }

        public static string GetIndentations(int indentation)
        {
            string str = "";
            while (indentation > 0)
            {
                str = str + "  ";
                indentation--;
            }
            return str;
        }

        public static void PrintStatement(ref Text fInput, ref StreamWriter fOutput, int iRecords, string strDefaultIndentation)
        {
            int indentations = 0;
            for (int r = 0; r < iRecords; r++)
            {
                long iOpCode = fInput.GetInt64();

                string strPrefixNeg = "";
                if ((iOpCode & 0x80000000) != 0)
                {
                    strPrefixNeg = "neg|";
                    iOpCode ^= 0x80000000;
                }
                string strPrefixThisOrNext = "";
                if ((iOpCode & 0x40000000) != 0)
                {
                    strPrefixThisOrNext = "this_or_next|";
                    iOpCode ^= 0x40000000;
                }

                if (iOpCode == 4 || iOpCode == 6 || iOpCode == 7 || iOpCode == 11 || iOpCode == 12 || iOpCode == 15 || iOpCode == 16 || iOpCode == 17 ||
                    iOpCode == 18)
                    indentations++;
                if (iOpCode == 3)
                    indentations--;

                var strIdentations = (iOpCode == 4 || iOpCode == 5 || iOpCode == 6 || iOpCode == 7 || iOpCode == 11 || iOpCode == 12 || iOpCode == 15 || iOpCode == 16 || iOpCode == 17 ||
                                      iOpCode == 18) ? GetIndentations(indentations - 1) : GetIndentations(indentations);

                string strOpCode = null;
                if (strPrefixNeg != "" && (iOpCode >= 30) && (iOpCode <= 32))
                {
                    switch (iOpCode)
                    {
                        case 30:
                            strOpCode = "lt";
                            break;
                        case 31:
                            strOpCode = "neq";
                            break;
                        case 32:
                            strOpCode = "le";
                            break;
                    }
                    fOutput.Write("{0}{1}({2}{3}", strIdentations, strDefaultIndentation, strPrefixThisOrNext, strOpCode);
                }
                else
                { 
                    try
                    {
                        strOpCode = Operations[iOpCode];
                    }
                    catch (Exception)
                    {
                        strOpCode = Convert.ToString(iOpCode);
                    }
                    fOutput.Write("{0}{1}({2}{3}{4}", strIdentations, strDefaultIndentation, strPrefixNeg, strPrefixThisOrNext, strOpCode); 
                }
                    

                int iParams = fInput.GetInt();
                for (int p = 0; p < iParams; p++)
                {
                    string strParam = fInput.GetWord();

					ulong lTemp;
					if(!UInt64.TryParse(strParam, out lTemp))
						goto lSkipParams;
#region Params
                    if ((iOpCode == 60) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //set_result_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 702) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //get_trigger_object_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 70) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //key_is_down
{
    strParam = GetKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 71) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //key_clicked
{
    strParam = GetKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 72) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //game_key_is_down
{
    strParam = GetGameKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 73) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //game_key_clicked
{
    strParam = GetGameKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 77) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //omit_key_once
{
    strParam = GetKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 75) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //mouse_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1275) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //faction_set_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1625) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //party_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1626) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //party_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1669) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //party_set_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1605) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //party_set_extra_text
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 980) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //context_menu_add_item
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1642) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //party_set_ai_target_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2292) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //party_get_ai_target_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1837) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //class_set_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1501) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //troop_set_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1502) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //troop_set_plural_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1291) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //setup_quest_giver
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 599) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //play_sound_at_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 701) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //init_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 700) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //copy_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 700) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //copy_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 719) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_copy_origin
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 719) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_copy_origin
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 718) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_copy_rotation
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 718) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_copy_rotation
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 716) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_parent
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 716) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_parent
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 716) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_parent
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 717) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_local
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 717) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_local
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 717) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //position_transform_position_to_local
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 726) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 727) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 728) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 729) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 730) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 731) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 720) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_move_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 721) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_move_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 722) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_move_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 791) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_z_to_ground_level
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 792) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_distance_to_terrain
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 793) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_distance_to_ground_level
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 742) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_rotation_around_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 743) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_rotation_around_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 740) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_rotation_around_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 723) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 724) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 725) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 738) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_x_floating
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 739) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_y_floating
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 735) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_scale_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 736) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_scale_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 737) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_scale_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 744) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_scale_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 745) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_scale_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 746) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_set_scale_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 705) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_angle_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 705) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_angle_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 707) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_has_line_of_sight_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 707) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_has_line_of_sight_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 710) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_distance_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 710) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_distance_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 711) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_distance_between_positions_in_meters
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 711) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_distance_between_positions_in_meters
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 712) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 712) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_positions
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 713) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_positions_in_meters
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 713) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_positions_in_meters
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 714) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_is_behind_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 714) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_is_behind_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 715) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_position_heights
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 715) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //get_sq_distance_between_position_heights
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 741) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_normalize_origin
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 750) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_get_screen_projection
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 750) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_screen_projection
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1627) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //map_get_random_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1627) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //map_get_random_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1628) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //map_get_land_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1628) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //map_get_land_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1629) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //map_get_water_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1629) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //map_get_water_position_around_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1117) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //add_troop_note_from_sreg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1118) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //add_faction_note_from_sreg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1119) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //add_party_note_from_sreg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1113) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //add_quest_note_from_sreg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1092) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //add_info_page_note_from_sreg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1980) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_tableau_mesh
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1988) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_set_camera_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1990) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_point_light
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1991) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_sun_light
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1992) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_mesh
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1993) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_mesh_with_vertex_color
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1994) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_map_icon
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1995) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_troop
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1996) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_horse
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2000) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_mesh_with_scale_and_vertex_color
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2000) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //cur_tableau_add_mesh_with_scale_and_vertex_color
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2318) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_is_empty
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2319) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_clear
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2320) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2320) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2321) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_string_reg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2321) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_string_reg
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2322) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_troop_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2323) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_troop_name_plural
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2324) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_troop_name_by_count
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2325) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_item_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2326) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_item_name_plural
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2327) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_item_name_by_count
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2330) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_party_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2332) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_agent_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2335) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_faction_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2336) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_quest_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2337) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_info_page_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2340) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_date
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2341) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_troop_name_link
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2342) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_party_name_link
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2343) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_faction_name_link
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2344) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_quest_name_link
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2345) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_info_page_name_link
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2350) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_player_username
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2351) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_server_password
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2352) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_server_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2353) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_welcome_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2355) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_encode_url
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1104) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //display_debug_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1105) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //display_log_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1106) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //display_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1120) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //tutorial_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1120) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //tutorial_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1120) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //dialog_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1120) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //dialog_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1121) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //question_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1121) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //question_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1121) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //question_box
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1122) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //tutorial_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1123) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //tutorial_message_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1123) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //tutorial_message_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2032) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //set_game_menu_tableau_mesh
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2022) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //talk_info_set_line
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1780) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //entry_point_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1781) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //entry_point_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1799) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //get_scene_boundaries
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1799) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //get_scene_boundaries
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2010) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //mission_cam_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2011) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //mission_cam_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2012) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //mission_cam_animate_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2016) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //mission_cam_animate_to_position_and_aperture
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1970) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //set_spawn_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1850) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1851) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_get_starting_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1855) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1860) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_animate_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1852) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_get_scale
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1865) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_rotate_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1871) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_dynamics_set_properties
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1872) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_dynamics_set_velocity
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1873) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_dynamics_set_omega
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1874) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_dynamics_apply_impulse
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1970) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //set_spawn_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1965) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //particle_system_add_new
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1969) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //particle_system_burst
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1975) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //particle_system_burst_no_sync
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1886) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //prop_instance_add_particle_system
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1826) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_is_in_line_of_sight
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1970) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //set_spawn_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1710) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1711) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1829) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //add_missile
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1689) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_get_speed
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1730) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_set_scripted_destination
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1748) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_set_scripted_destination_no_attack
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1731) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_get_scripted_destination
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1709) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_get_look_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1744) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_set_look_target_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1751) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_start_running_away
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1791) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //team_set_order_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 1794) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //team_get_order_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 910) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //create_text_overlay
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 912) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //create_button_overlay
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 940) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //create_game_button_overlay
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 941) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //create_in_game_button_overlay
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 943) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //create_listbox_overlay
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 931) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //overlay_add_item
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 946) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //overlay_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 920) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //overlay_set_text
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 926) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //overlay_set_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 925) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //overlay_set_size
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 929) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //overlay_set_area_size
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 937) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //overlay_animate_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 936) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //overlay_animate_to_size
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 930) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //overlay_set_mesh_rotation
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 950) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //overlay_set_tooltip
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 970) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //show_item_details
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 972) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //show_item_details_with_modifier
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2388) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //show_troop_details
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 380) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //send_message_to_url
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 393) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //multiplayer_send_string_to_server
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 399) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //multiplayer_send_string_to_player
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 473) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //server_add_message_to_log
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 484) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //server_set_password
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 489) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //server_set_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 492) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //server_set_welcome_message
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 1130) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //set_tooltip_text
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 12) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //try_for_agents
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 18) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //try_for_dict_keys
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 723) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 724) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 725) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 738) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_x_floating
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 739) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_y_floating
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 2600) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_equals
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2600) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_equals
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2601) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_contains
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2601) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_contains
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2602) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_starts_with
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2602) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_starts_with
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2603) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_ends_with
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2603) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_ends_with
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2604) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_is_alpha
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2605) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_is_digit
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2606) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_is_whitespace
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2607) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_length
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2608) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_index_of
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2608) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //str_index_of
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2609) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_last_index_of
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2609) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //str_last_index_of
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2610) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_get_char
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2611) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_to_num
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2612) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_compare
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2612) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //str_compare
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2613) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_split
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2613) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //str_split
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2614) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_sort
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2615) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_lower
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2615) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_lower
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2616) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_upper
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2616) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_upper
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2617) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_trim
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2617) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_trim
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2618) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2618) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2618) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2618) && p == 3 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2619) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_md5
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2619) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_md5
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2620) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_substring
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2620) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_substring
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2621) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_reverse
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2621) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_reverse
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2622) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_join
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2622) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_join
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2623) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace_spaces_with_underscores
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2623) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace_spaces_with_underscores
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2624) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace_underscores_with_spaces
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2624) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_replace_underscores_with_spaces
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2625) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_multiplayer_profile_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2626) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_face_keys
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2626) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //str_store_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2627) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_module_setting
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2628) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_server_password_admin
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2629) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_server_password_private
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2630) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_overlay_text
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2631) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_player_ip
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2632) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_game_variable
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2633) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_skill_name
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2634) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_float
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2634) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //str_store_float
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 2635) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_sanitize
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2636) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_store_item_id
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2637) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //str_is_integer
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2700) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_init
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2701) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_copy
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2701) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_copy
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2702) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_hair
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2703) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_hair
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2704) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_beard
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2705) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_beard
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2706) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_face_texture
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2707) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_face_texture
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2708) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_hair_texture
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2709) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_hair_texture
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2710) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_hair_color
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2711) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_hair_color
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2712) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_age
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2713) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_age
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2714) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_get_morph_key
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2715) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_set_morph_key
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2716) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //face_keys_store_string
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2716) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //face_keys_store_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 2900) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //player_get_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 2901) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //player_set_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 3019) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //send_message_to_url_advanced
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3019) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //send_message_to_url_advanced
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3100) && p == 1 && IsKey(Convert.ToUInt64(strParam))) //game_key_get_key
{
    strParam = GetGameKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 3102) && p == 0 && IsKey(Convert.ToUInt64(strParam))) //game_key_released
{
    strParam = GetGameKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 3210) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //dict_get_str
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3212) && p == 2 && IsStringRegister(Convert.ToUInt64(strParam))) //dict_set_str
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3310) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_get_bone_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3314) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //agent_ai_get_move_target_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3317) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //agent_accelerate
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3405) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //multiplayer_profile_get_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 3407) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //multiplayer_message_put_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3409) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //multiplayer_message_put_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3410) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //multiplayer_message_put_coordinate
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3412) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //multiplayer_cur_message_get_string
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 3414) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //multiplayer_cur_message_get_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3415) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //multiplayer_cur_message_get_coordinate
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3608) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //get_camera_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3614) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //cur_missile_get_path_point_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3616) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //cast_ray
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3616) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //cast_ray
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 3706) && p == 0 && IsFaceKey(Convert.ToUInt64(strParam))) //troop_get_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 3707) && p == 1 && IsFaceKey(Convert.ToUInt64(strParam))) //troop_set_face_keys
{
    strParam = GetFaceKey(Convert.ToUInt64(strParam));
}
else if ((iOpCode == 4100) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_rotate_z_floating
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4101) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //position_get_vector_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4101) && p == 2 && IsPosition(Convert.ToUInt64(strParam))) //position_get_vector_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4101) && p == 3 && IsPosition(Convert.ToUInt64(strParam))) //position_get_vector_to_position
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4102) && p == 0 && IsPosition(Convert.ToUInt64(strParam))) //position_align_to_ground
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4400) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fld
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4401) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fld_str
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4401) && p == 1 && IsStringRegister(Convert.ToUInt64(strParam))) //fld_str
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 4402) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fld_pos_x
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4402) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //fld_pos_x
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4403) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fld_pos_y
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4403) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //fld_pos_y
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4404) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fld_pos_z
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4404) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //fld_pos_z
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4405) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fst
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4406) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcpy
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4406) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcpy
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4407) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //feq
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4407) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //feq
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4407) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //feq
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4408) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fgt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4408) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fgt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4408) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fgt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4409) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //flt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4409) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //flt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4409) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //flt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4410) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fge
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4410) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fge
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4410) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fge
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4411) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fle
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4411) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fle
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4411) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fle
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4412) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsub
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4412) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsub
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4412) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsub
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4413) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmul
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4413) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmul
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4413) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmul
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4414) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fdiv
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4414) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fdiv
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4414) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fdiv
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4415) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4415) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4415) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4416) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmax
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4416) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmax
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4416) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmax
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4417) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fclamp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4417) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fclamp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4417) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fclamp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4417) && p == 3 && IsFloatRegister(Convert.ToUInt64(strParam))) //fclamp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4418) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsqrt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4418) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsqrt
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4419) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fabs
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4419) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fabs
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4420) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fceil
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4420) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fceil
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4421) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //ffloor
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4421) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //ffloor
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4422) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fexp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4422) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fexp
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4423) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fpow
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4423) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fpow
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4423) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fpow
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4424) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fln
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4424) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fln
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4425) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //flog
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4425) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //flog
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4426) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmod
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4426) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmod
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4426) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fmod
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4427) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4427) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4428) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcos
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4428) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcos
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4429) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //ftan
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4429) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //ftan
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4430) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsinh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4430) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fsinh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4431) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcosh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4431) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fcosh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4432) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //ftanh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4432) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //ftanh
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4433) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fasin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4433) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fasin
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4434) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //facos
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4434) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //facos
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4435) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fatan
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4435) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fatan
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4436) && p == 0 && IsFloatRegister(Convert.ToUInt64(strParam))) //fatan2
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4436) && p == 1 && IsFloatRegister(Convert.ToUInt64(strParam))) //fatan2
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4436) && p == 2 && IsFloatRegister(Convert.ToUInt64(strParam))) //fatan2
{
    strParam = "fp" + strParam;
}
else if ((iOpCode == 4437) && p == 0 && IsStringRegister(Convert.ToUInt64(strParam))) //feval
{
    strParam = "s" + strParam;
}
else if ((iOpCode == 4702) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //set_shader_param_vector
{
    strParam = "pos" + strParam;
}
else if ((iOpCode == 4703) && p == 1 && IsPosition(Convert.ToUInt64(strParam))) //set_shader_param_matrix
{
    strParam = "pos" + strParam;
}

                    //Text:
                    else if ((iOpCode == 910) && p == 2 && IsTextFlags(Convert.ToUInt64(strParam)))
                    {
                        strParam = DecompileTextFlags(Convert.ToUInt32(strParam));
                    }
#endregion

					lSkipParams:
                    fOutput.Write(", {0}", UInt64.TryParse(strParam, out lTemp) ? GetParam(Convert.ToUInt64(strParam)) : strParam);
                }
                fOutput.WriteLine("),");

            }
        }

        public static string GetKey(ulong lKeyCode)
        {
            switch (lKeyCode)
            {
                case 0x02: return "key_1";
                case 0x03: return "key_2";
                case 0x04: return "key_3";
                case 0x05: return "key_4";
                case 0x06: return "key_5";
                case 0x07: return "key_6";
                case 0x08: return "key_7";
                case 0x09: return "key_8";
                case 0x0a: return "key_9";
                case 0x0b: return "key_0";
                case 0x1e: return "key_a";
                case 0x30: return "key_b";
                case 0x2e: return "key_c";
                case 0x20: return "key_d";
                case 0x12: return "key_e";
                case 0x21: return "key_f";
                case 0x22: return "key_g";
                case 0x23: return "key_h";
                case 0x17: return "key_i";
                case 0x24: return "key_j";
                case 0x25: return "key_k";
                case 0x26: return "key_l";
                case 0x32: return "key_m";
                case 0x31: return "key_n";
                case 0x18: return "key_o";
                case 0x19: return "key_p";
                case 0x10: return "key_q";
                case 0x13: return "key_r";
                case 0x1f: return "key_s";
                case 0x14: return "key_t";
                case 0x16: return "key_u";
                case 0x2f: return "key_v";
                case 0x11: return "key_w";
                case 0x2d: return "key_x";
                case 0x15: return "key_y";
                case 0x2c: return "key_z";
                case 0x52: return "key_numpad_0";
                case 0x4f: return "key_numpad_1";
                case 0x50: return "key_numpad_2";
                case 0x51: return "key_numpad_3";
                case 0x4b: return "key_numpad_4";
                case 0x4c: return "key_numpad_5";
                case 0x4d: return "key_numpad_6";
                case 0x47: return "key_numpad_7";
                case 0x48: return "key_numpad_8";
                case 0x49: return "key_numpad_9";
                case 0x45: return "key_num_lock";
                case 0xb5: return "key_numpad_slash";
                case 0x37: return "key_numpad_multiply";
                case 0x4a: return "key_numpad_minus";
                case 0x4e: return "key_numpad_plus";
                case 0x9c: return "key_numpad_enter";
                case 0x53: return "key_numpad_period";
                case 0xd2: return "key_insert";
                case 0xd3: return "key_delete";
                case 0xc7: return "key_home";
                case 0xcf: return "key_end";
                case 0xc9: return "key_page_up";
                case 0xd1: return "key_page_down";
                case 0xc8: return "key_up";
                case 0xd0: return "key_down";
                case 0xcb: return "key_left";
                case 0xcd: return "key_right";
                case 0x3b: return "key_f1";
                case 0x3c: return "key_f2";
                case 0x3d: return "key_f3";
                case 0x3e: return "key_f4";
                case 0x3f: return "key_f5";
                case 0x40: return "key_f6";
                case 0x41: return "key_f7";
                case 0x42: return "key_f8";
                case 0x43: return "key_f9";
                case 0x44: return "key_f10";
                case 0x57: return "key_f11";
                case 0x58: return "key_f12";
                case 0x39: return "key_space";
                case 0x01: return "key_escape";
                case 0x1c: return "key_enter";
                case 0x0f: return "key_tab";
                case 0x0e: return "key_back_space";
                case 0x1a: return "key_open_braces";
                case 0x1b: return "key_close_braces";
                case 0x33: return "key_comma";
                case 0x34: return "key_period";
                case 0x35: return "key_slash";
                case 0x2b: return "key_back_slash";
                case 0x0d: return "key_equals";
                case 0x0c: return "key_minus";
                case 0x27: return "key_semicolon";
                case 0x28: return "key_apostrophe";
                case 0x29: return "key_tilde";
                case 0x3a: return "key_caps_lock";
                case 0x2a: return "key_left_shift";
                case 0x36: return "key_right_shift";
                case 0x1d: return "key_left_control";
                case 0x9d: return "key_right_control";
                case 0x38: return "key_left_alt";
                case 0xb8: return "key_right_alt";
                case 0xe0: return "key_left_mouse_button";
                case 0xe1: return "key_right_mouse_button";
                case 0xe2: return "key_middle_mouse_button";
                case 0xe3: return "key_mouse_button_4";
                case 0xe4: return "key_mouse_button_5";
                case 0xe5: return "key_mouse_button_6";
                case 0xe6: return "key_mouse_button_7";
                case 0xe7: return "key_mouse_button_8";
                case 0xee: return "key_mouse_scroll_up";
                case 0xef: return "key_mouse_scroll_down";
                case 0xf0: return "key_xbox_a";
                case 0xf1: return "key_xbox_b";
                case 0xf2: return "key_xbox_x";
                case 0xf3: return "key_xbox_y";
                case 0xf4: return "key_xbox_dpad_up";
                case 0xf5: return "key_xbox_dpad_down";
                case 0xf6: return "key_xbox_dpad_right";
                case 0xf7: return "key_xbox_dpad_left";
                case 0xf8: return "key_xbox_start";
                case 0xf9: return "key_xbox_back";
                case 0xfa: return "key_xbox_rbumber";
                case 0xfb: return "key_xbox_lbumber";
                case 0xfc: return "key_xbox_ltrigger";
                case 0xfd: return "key_xbox_rtrigger";
                case 0xfe: return "key_xbox_rstick";
                case 0xff: return "key_xbox_lstick";
                default: return String.Format("0x{0:x}", lKeyCode);
            }          
        }

        public static string GetGameKey(ulong lKeyCode)
        {
            switch (lKeyCode)
            {
                case 0: return "gk_move_forward";
                case 1: return "gk_move_backward";
                case 2: return "gk_move_left";
                case 3: return "gk_move_right";
                case 4: return "gk_action";
                case 5: return "gk_jump";
                case 6: return "gk_attack";
                case 7: return "gk_defend";
                case 8: return "gk_kick";
                case 9: return "gk_toggle_weapon_mode";
                case 10: return "gk_equip_weapon_1";
                case 11: return "gk_equip_weapon_2";
                case 12: return "gk_equip_weapon_3";
                case 13: return "gk_equip_weapon_4";
                case 14: return "gk_equip_primary_weapon";
                case 15: return "gk_equip_secondary_weapon";
                case 16: return "gk_drop_weapon";
                case 17: return "gk_sheath_weapon";
                case 18: return "gk_leave";
                case 19: return "gk_zoom";
                case 20: return "gk_view_char";
                case 21: return "gk_cam_toggle";
                case 22: return "gk_view_orders";
                case 23: return "gk_order_1";
                case 24: return "gk_order_2";
                case 25: return "gk_order_3";
                case 26: return "gk_order_4";
                case 27: return "gk_order_5";
                case 28: return "gk_order_6";
                case 29: return "gk_everyone_hear";
                case 30: return "gk_infantry_hear";
                case 31: return "gk_archers_hear";
                case 32: return "gk_cavalry_hear";
                case 33: return "gk_group3_hear";
                case 34: return "gk_group4_hear";
                case 35: return "gk_group5_hear";
                case 36: return "gk_group6_hear";
                case 37: return "gk_group7_hear";
                case 38: return "gk_group8_hear";
                case 39: return "gk_reverse_order_group";
                case 40: return "gk_everyone_around_hear";
                case 41: return "gk_mp_message_all";
                case 42: return "gk_mp_message_team";
                case 43: return "gk_character_window";
                case 44: return "gk_inventory_window";
                case 45: return "gk_party_window";
                case 46: return "gk_quests_window";
                case 47: return "gk_game_log_window";
                case 48: return "gk_quick_save";
                case 49: return "gk_crouch";
                case 50: return "gk_order_7";
                case 51: return "gk_order_8";
                default: return String.Format("0x{0:x}", lKeyCode);
            }
        }

        public static bool IsStringRegister(ulong lParam)
        {
            ulong lTag = (lParam & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }

        public static bool IsKey(ulong lKeyCode)
        {
            ulong lTag = (lKeyCode & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }

        public static bool IsTextFlags(ulong lFlags)
        {
            ulong lTag = (lFlags & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }
		
        public static bool IsPosition(ulong lFlags)
        {
            ulong lTag = (lFlags & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }
		
        public static bool IsFloatRegister(ulong lFlags)
        {
            ulong lTag = (lFlags & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }
		
        public static bool IsFaceKey(ulong lFlags)
        {
            ulong lTag = (lFlags & 0xFF00000000000000) >> 56;
            return lTag == 0;
        }

        public static string GetFaceKey(ulong lFaceKeyCode)
        {
            return Convert.ToString(lFaceKeyCode);
        }

        public static string DecompileTextFlags(DWORD dwFlag)
        {
            string strFlag = "";

            string[] strFlags = { "tf_left_align", "tf_right_align", "tf_center_justify", "tf_double_space", "tf_vertical_align_center", "tf_scrollable", 
            "tf_single_line", "tf_with_outline", "tf_scrollable_style_2" };
            DWORD[] dwFlags = { 0x00000004, 0x00000008, 0x00000010, 0x00000800, 0x00001000, 0x00002000, 0x00008000, 0x00010000, 0x00020000 };
            for (int i = 0; i < (dwFlags.Length); i++)
            {
                if ((dwFlag & dwFlags[i]) != 0)
                {
                    strFlag += strFlags[i] + "|";
                    dwFlag ^= dwFlags[i];
                }
            }

            strFlag = strFlag == "" ? "0" : strFlag.Remove(strFlag.Length - 1, 1);

            return strFlag;
        }
    }
}
