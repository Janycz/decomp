﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Decomp
{
    public class Text
    {
        private readonly StreamReader _reader;
        public Text(string fileName)
        {
            _reader = new StreamReader(fileName, Encoding.ASCII);
        }

        ~Text()
        {
            _reader.Dispose();
        }

        public string GetWord()
        {
            int i;
            do
            {
                i = _reader.Read();
            }while (char.IsWhiteSpace((char)i)); //pass spaces

            var c = (char)(i);
            var sb = new StringBuilder();

            do
            {
                sb.Append(c);
                i = _reader.Peek();
                if (i == -1)
                    break;
                c = (char)(i);
                if (char.IsWhiteSpace(c))
                    break;
                _reader.Read();
            }while (true);

            return sb.ToString();
        }

        public long GetInt64()
        {
            long x;
            try
            {
                x = Convert.ToInt64(GetWord());
            }
            catch (Exception)
            {
                x = 0;
            }
            return x;
        }

        public ulong GetUInt64()
        {
            ulong x;
            try
            {
                x = Convert.ToUInt64(GetWord());
            }
            catch (Exception)
            {
                x = 0;
            }
            return x;
        }

        public int GetInt()
        {
            return (int)GetInt64();
        }
        
        public uint GetUInt()
        {
            return (uint)GetUInt64();
        }

        public uint GetDWord()
        {
            return (uint)GetUInt64();
        }
  
        public double GetDouble()
        {
            double x;
            try
            {
                x = Convert.ToDouble(GetWord(), new CultureInfo("en-US"));
            }
            catch (Exception)
            {
                x = 0.0;
            }
            return x;
        }

        public string GetString()
        {
            return _reader.ReadLine();
        }

        public void Close()
        {
            _reader.Close();
        }
    }
}
