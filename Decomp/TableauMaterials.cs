﻿using System.IO;

namespace Decomp
{
    public static class TableauMaterials
    {
        public static void Decompile()
        {
            var fTableaus = new Text(Common.InputPath + @"\tableau_materials.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_tableau_materials.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.TableauMaterialsHeader);
            int iCount = fTableaus.GetInt();

            for (int i = 0; i < iCount; i++)
            {
                fSource.Write("  (\"{0}\", {1}, \"{2}\", ", fTableaus.GetWord().Remove(0, 4), fTableaus.GetDWord(), fTableaus.GetWord());
                for (int j = 0; j < 6; j++)
                    fSource.Write(" {0},", fTableaus.GetInt());
                fSource.WriteLine("\r\n  [");
                Common.PrintStatement(ref fTableaus, ref fSource, fTableaus.GetInt(), "    ");
                fSource.WriteLine("  ]),\r\n");
            }
            fSource.Write("]");
            fSource.Close();
            fTableaus.Close();
        }
    }
}
