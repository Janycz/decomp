﻿using System;
using System.Collections.Generic;
using System.IO;
using DWORD = System.UInt32;
using DWORD64 = System.UInt64;

namespace Decomp
{
    public static class Troops
    {
        private static string GetScene(DWORD dwScene)
        {
            DWORD dwEntry = ((dwScene & 0xFFFF0000) >> 16);
            DWORD dwID = dwScene & 0xFFFF;

            return "scn_" + Common.Scenes[dwID] + "|entry(" + dwEntry + ")";
        }

        public static string DecompileFlags(DWORD dwFlag)
        {
            string strFlag = "";

            DWORD dwSkin = dwFlag & 0xF;
            if(dwSkin > 0)
                strFlag = "tf_" + Common.Skins[dwSkin] + "|";

            if (((dwFlag & 0x7F00000) - 0x7F00000) == 0)
            {
                strFlag += "tf_guarantee_all|";
                dwFlag ^= 0x7F00000;
            }
            else if (((dwFlag & 0x3F00000) - 0x3F00000) == 0)
            {
                strFlag += "tf_guarantee_all_wo_ranged";
                dwFlag ^= 0x3F00000;
            }
            string[] strFlags = { "tf_hero", "tf_inactive", "tf_unkillable", "tf_allways_fall_dead", "tf_no_capture_alive", "tf_mounted", 
            "tf_is_merchant", "tf_randomize_face", "tf_guarantee_boots", "tf_guarantee_armor", "tf_guarantee_helmet", "tf_guarantee_gloves", 
            "tf_guarantee_horse", "tf_guarantee_shield", "tf_guarantee_ranged", "tf_unmoveable_in_party_window" };
            DWORD[] dwFlags = { 0x00000010, 0x00000020, 0x00000040, 0x00000080, 0x00000100, 0x00000400, 0x00001000, 0x00008000, 0x00100000, 
            0x00200000, 0x00400000, 0x00800000, 0x01000000, 0x02000000, 0x04000000, 0x10000000 };
            for (int i = 0; i < (dwFlags.Length); i++)
            {
                if ((dwFlag & dwFlags[i]) != 0)
                {
                    strFlag += strFlags[i] + "|";
                    dwFlag ^= dwFlags[i];
                }
            }

            strFlag = strFlag == "" ? "0" : strFlag.Remove(strFlag.Length - 1, 1);

            return strFlag;
        }

        public static void Decompile()
        {
            var fTroops = new Text(Common.InputPath + @"\troops.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_troops.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.TroopsHeader);

            for (int s = 0; s < Common.Skins.Length; s++)
            {
                fSource.WriteLine("tf_" + Common.Skins[s] + " = " + s);
            }
            fSource.WriteLine("\r\ntroops = [");
            
            fTroops.GetString();
            int iTroops = fTroops.GetInt();

            var strUpList = new List<string>();

            for (int t = 0; t < iTroops; t++)
            {
                fSource.Write("  [\"{0}\", \"{1}\", \"{2}\",", fTroops.GetWord().Remove(0, 4), fTroops.GetWord().Replace('_', ' '), fTroops.GetWord().Replace('_', ' '));
                fTroops.GetWord();

                DWORD dwFlag = fTroops.GetDWord();
                fSource.Write(" {0},", DecompileFlags(dwFlag));

                DWORD dwScene = fTroops.GetDWord();
                fSource.Write(" {0},", dwScene == 0 ? "0" : GetScene(dwScene));

                fSource.Write(" {0},", fTroops.GetWord()); // reserved "0"

                int iFaction = fTroops.GetInt();
                if (iFaction > 0 && iFaction < Common.Factions.Length)
                    fSource.WriteLine(" fac_{0},", Common.Factions[iFaction]);
                else
                    fSource.WriteLine(" {0},", iFaction);

                int iUp1 = fTroops.GetInt();
                int iUp2 = fTroops.GetInt();

                if (iUp1 != 0 && iUp2 != 0)
                    strUpList.Add(String.Format("upgrade2(troops,\"{0}\",\"{1}\",\"{2}\")", Common.Troops[t], Common.Troops[iUp1], Common.Troops[iUp2]));
                else if (iUp1 != 0 && iUp2 == 0)
                    strUpList.Add(String.Format("upgrade(troops,\"{0}\",\"{1}\")", Common.Troops[t], Common.Troops[iUp1]));

                string strItemList = "";
                for (int i = 0; i < 64; i++)
                {
                    int iItem = fTroops.GetInt();
                    fTroops.GetInt(); //skip 0
                    if (-1 == iItem)
                        continue;
                    strItemList += String.Format("itm_{0},", Common.Items[iItem]);
                }
                if (strItemList.Length > 0)
                    strItemList = strItemList.Remove(strItemList.Length - 1, 1);
                fSource.WriteLine("  [{0}],", strItemList);

                int iStregth = fTroops.GetInt(),
                    iAgility = fTroops.GetInt(),
                    iIntelligence = fTroops.GetInt(),
                    iCharisma = fTroops.GetInt(),
                    iLevel = fTroops.GetInt();

                fSource.Write("  str_{0}|agi_{1}|int_{2}|cha_{3}|level({4}), ", iStregth, iAgility, iIntelligence, iCharisma, iLevel);

                var iWP = new int[7];
                for (int i = 0; i < 7; i++)
                    iWP[i] = fTroops.GetInt();

                if (iWP[0] == iWP[1] && iWP[1] == iWP[2] && iWP[2] == iWP[3] && iWP[3] == iWP[4] && iWP[4] == iWP[5])
                    fSource.Write("wp({0}){1},", iWP[0], iWP[6] == 0 ? "" : "|wp_firearm(" + iWP[6] + ")");
                else if (iWP[0] == iWP[1] && iWP[1] == iWP[2])
                    fSource.Write("wpe({0},{1},{2},{3}){4},", iWP[0], iWP[3], iWP[4], iWP[5], iWP[6] == 0 ? "" : "|wp_firearm(" + iWP[6] + ")");
                else
                    fSource.Write("wpex({0},{1},{2},{3},{4},{5}){6},", iWP[0], iWP[1], iWP[2], iWP[3], iWP[4], iWP[5], iWP[6] == 0 ? "" : "|wp_firearm(" + iWP[6] + ")");

                string strKnow = "";
                for (int x = 0; x < 6; x++)
                {
                    DWORD dword = fTroops.GetDWord();
                    if (dword == 0)
                        continue;
                    for (int q = 0; q < 8; q++)
                    {
                        DWORD dwKnow = (0xF & (dword >> (q * 4)));
                        /*if (dwKnow != 0 && dwKnow <= 8)
                            strKnow = strKnow + String.Format("knows_{0}_{1}|", Common.Skills[x * 8 + q], dwKnow);
                        else*/ 
                        if (dwKnow != 0)
                            strKnow += String.Format("knows_{0}_{1}|", Common.Skills[x * 8 + q], dwKnow /* - 8*/);
                    }
                }
                strKnow = strKnow == "" ? "0" : strKnow.Remove(strKnow.Length - 1, 1);
                fSource.Write(" {0},", strKnow);

                string strFase = String.Format("0x{0:x16}{1:x16}{2:x16}{3:x16}, 0x{4:x16}{5:x16}{6:x16}{7:x16}", fTroops.GetUInt64(), fTroops.GetUInt64(),
                                     fTroops.GetUInt64(), fTroops.GetUInt64(), fTroops.GetUInt64(), fTroops.GetUInt64(), fTroops.GetUInt64(), fTroops.GetUInt64());
                fSource.WriteLine("{0}],", strFase);
            }

            fSource.WriteLine("]");
            foreach (var strUp in strUpList)
            {
                fSource.WriteLine(strUp);
            }
            fSource.Close();
            fTroops.Close();
        }
    }
}
