﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Decomp
{
    public static class Meshes
    {
        public static void Decompile()
        {
            var fMeshes = new Text(Common.InputPath + @"\meshes.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_meshes.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.MeshesHeader);
            int iMeshes = fMeshes.GetInt();
            for (int m = 0; m < iMeshes; m++)
            {
                fSource.Write("  (\"{0}\", ", fMeshes.GetWord().Remove(0, 5));

                int iFlag = fMeshes.GetInt();
                if (iFlag == 1)
                    fSource.Write("render_order_plus_1,");
                else
                    fSource.Write("{0},", iFlag);

                fSource.Write(" \"{0}\"", fMeshes.GetWord());
                
                for (int i = 0; i < 9; i++)
                    fSource.Write(", {0}", fMeshes.GetDouble().ToString(CultureInfo.GetCultureInfo("en-US")));
                fSource.WriteLine("),");
            }
            fSource.Write("]");
            fSource.Close();
            fMeshes.Close();
        }
    }
}
