﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Threading;
using Microsoft.Win32;
using Button = System.Windows.Controls.Button;
using CheckBox = System.Windows.Controls.CheckBox;

namespace Decomp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        [DllImport("kernel32.dll", EntryPoint = "FormatMessageW", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern int FormatMessage(
            FormatMessageFlags dwFlags,
            IntPtr lpSource,
            int dwMessageId,
            int dwLanguageId,
            StringBuilder lpBuffer,
            int nSize,
            IntPtr[] lpArguments
        );

        [Flags]
        public enum FormatMessageFlags
        {
            AllocateBuffer = 0x100,
            ArgumentArray = 0x2000,
            FromHmodule = 0x800,
            FromString = 0x400,
            FromSystem = 0x1000,
            IgnoreInserts = 0x200,
            MaxWidthMask = 0xff
        }

        public static string GetPath(DirectoryNotFoundException hException)
        {
            string strMessage = hException.Message;
            int iBeginPos = strMessage.IndexOf('"');
            int iEndPos = strMessage.IndexOf('"', iBeginPos + 1);
            return Path.GetDirectoryName(strMessage.Substring(iBeginPos + 1, iEndPos - iBeginPos - 1));
        }

        private Thread _hWorkThread;

        private static readonly Action EmptyDelegate = delegate { };
        public static void Refresh(UIElement uiElement)
        {
            uiElement.Dispatcher.Invoke(DispatcherPriority.Render, EmptyDelegate);
        }

        public void Print(string strMsg)
        {
            textBox3.Dispatcher.Invoke(delegate { textBox3.Text += strMsg; });
        }

        public void Print(string strMsg, params object[] aObjects)
        {
            if (aObjects == null)
                Print(strMsg);
            else
                textBox3.Dispatcher.Invoke(delegate { textBox3.Text += String.Format(strMsg, aObjects); });
        }

        public void InitPath()
        {
            textBox3.Dispatcher.Invoke(delegate
            {
                Common.InputPath = textBox1.Text;
                Common.OutputPath = textBox2.Text;
            });
        }

        public void SetButtonText(Button hButton, string strText)
        {
            hButton.Dispatcher.Invoke(delegate { hButton.Content = strText; });
        }

        public bool IsCheckedCheckBox(UIElement uiElement)
        {
            bool bChecked = false;
            uiElement.Dispatcher.Invoke(delegate { bChecked = (uiElement as CheckBox).IsChecked ?? false; });
            return bChecked;
        }

        protected void InitUI()
        {
            try
            {
                RegistryKey hKey = Registry.CurrentUser;
                hKey = hKey.OpenSubKey("Software\\WMD");

                string strSource = hKey.GetValue("LastSource").ToString().Trim(),
                       strOutput = hKey.GetValue("LastOutput").ToString().Trim();

                if (strSource.Length > 0)
                    if (strSource[strSource.Length - 1] == '\\')
                        strSource = strSource.Remove(strSource.Length - 1, 1);
                if (strOutput.Length > 0)
                    if (strOutput[strOutput.Length - 1] == '\\')
                        strOutput = strOutput.Remove(strOutput.Length - 1, 1);

                textBox1.Text = strSource;
                textBox2.Text = strOutput;

                checkBox1.IsChecked = Convert.ToUInt32(hKey.GetValue("OpenAfterComplete").ToString()) != 0U;
            }
            catch (Exception)
            {
                RegistryKey hKey = Registry.CurrentUser.OpenSubKey("Software", true);
                hKey.CreateSubKey("WMD");
            }
        }

        protected void SaveUI()
        {
            using (RegistryKey hKey = Registry.CurrentUser.OpenSubKey("Software\\WMD", true))
            {
                hKey.SetValue("LastSource", textBox1.Text + "\\");
                hKey.SetValue("LastOutput", textBox2.Text + "\\");
                hKey.SetValue("OpenAfterComplete", IsCheckedCheckBox(checkBox1), RegistryValueKind.DWord);
            }
        }

        public MainWindow()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            InitializeComponent();
            _hWorkThread = new Thread(Decompile);
        }

        private void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DecompileButtonClick(object sender, RoutedEventArgs e)
        {
            if (_hWorkThread.IsAlive)
            {
                button4.Content = "Decompile";
                _hWorkThread.Abort();
            }
            else
            {
                textBox3.Text = "";
                _hWorkThread = new Thread(Decompile);
                if (textBox1.Text.Trim() == "") textBox1.Text = Environment.CurrentDirectory;
                if (textBox2.Text.Trim() == "") textBox2.Text = Environment.CurrentDirectory;
                _hWorkThread.Start();
            }
        }

        protected void Decompile()
        {
            SetButtonText(button4, "Stop");
            //button4.Dispatcher.Invoke(new Action(delegate { button4.Content = "Stop"; }));

            var sw = Stopwatch.StartNew();
            //Initialisation
            Print("Initialisation ");
            InitPath();

            try
            {
                Common.Procedures = InitScripts();
                Common.QuickStrings = InitQuickStrings();
                Common.Strings = InitStrings();
                Common.Items = InitItems();
                Common.Troops = InitTroops();
                Common.Factions = InitFactions();
                Common.Quests = InitQuests();
                Common.PTemps = InitPartyTemplates();
                Common.Parties = InitParties();
                Common.Menus = InitMenus();
                Common.Sounds = InitSounds();
                Common.Skills = InitSkills();
                Common.Meshes = InitMeshes();
                Common.Variables = InitVariables();
                Common.DialogStates = InitDialogStates();
                Common.Scenes = InitScenes();
                Common.MissionTemplates = InitMissionTemplates();
                Common.ParticleSystems = InitParticles();
                Common.SceneProps = InitSceneProps();
                Common.MapIcons = InitMapIcons();
                Common.Presentations = InitPresentations();
                Common.Tableaus = InitTableauMateriales();
                Common.Animations = InitAnimations();
                Common.Music = InitMusic();
                Common.Skins = InitSkins();
            }
            catch (FileNotFoundException ex)
            {
                Print("\nFile \"{0}\" not found\nDecompilation Aborted", ex.FileName);
                SetButtonText(button4, "Decompile");
                return;
            }
            catch (DirectoryNotFoundException ex)
            {
                Print("\nDirectory \"{0}\" not found\nDecompilation Aborted", GetPath(ex));
                SetButtonText(button4, "Decompile");
                return;
            }
            catch(Exception ex)
            {
                var szErrorMessage = new StringBuilder(2048);
                FormatMessage(
                    FormatMessageFlags.IgnoreInserts | FormatMessageFlags.FromSystem,
                    IntPtr.Zero,
                    ex.HResult,
                    new CultureInfo("en-US").LCID,
                    szErrorMessage,
                    szErrorMessage.Capacity,
                    null);

                Print("\n{0}Decompilation Aborted", szErrorMessage);
                SetButtonText(button4, "Decompile");
                return;
            }

            Common.Operations = new string[10000];
            foreach (var cmd in Commands.CArray)
            {
                Common.Operations[(int)cmd.ID] = cmd.Name;
            }

            File.WriteAllText(Common.OutputPath + @"\module_constants.py", Header.StandardHeader + Common.ModuleConstantsText);

            double dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency);

            Print("actions.txt ");
            Animations.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("dialogs.txt ");
            Dialogs.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("factions.txt ");
            Factions.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\Data\flora_kinds.txt"))
            {
                Print("flora_kinds.txt ");
                Flora.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            }

            if (File.Exists(Common.InputPath + @"\info_pages.txt"))
            {
                Print("info_pages.txt ");
                InfoPages.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            }
            else 
                Print("File \"{0}\\info_pages.txt\" not found\n", Common.InputPath);

            Print("items.txt ");
            Items.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\Data\ground_specs.txt"))
            {
                Print("ground_specs.txt ");
                GroundSpecs.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            }

            Print("map_icons.txt ");
            MapIcons.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("menus.txt ");
            Menus.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("meshes.txt ");
            Meshes.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("mission_templates.txt ");
            MissionTemplates.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("music.txt ");
            Music.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("particle_systems.txt ");
            ParticleSystems.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("parties.txt ");
            Parties.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("party_templates.txt ");
            PartyTemplates.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\postfx.txt"))
            {
                Print("postfx.txt ");
                Postfx.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            }
            else
                Print("File \"{0}\\postfx.txt\" not found\n", Common.InputPath);

            Print("presentations.txt ");
            Presentations.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("quests.txt ");
            Quests.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("scene_props.txt ");
            SceneProps.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("scenes.txt ");
            Scenes.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("scripts.txt ");
            Scripts.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\simple_triggers.txt"))
            {
                Print("simple_triggers.txt ");
                SimpleTriggers.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks*1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks*1000.0 / Stopwatch.Frequency;
            }
            else
                Print("File \"{0}\\simple_triggers.txt\" not found\n", Common.InputPath);

            Print("skills.txt ");
            Skills.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("skins.txt ");
            Skins.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\Data\skyboxes.txt"))
            {
                Print("skyboxes.txt ");
                Skyboxes.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;
            }

            Print("sounds.txt ");
            Sounds.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("strings.txt ");
            Strings.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            Print("tableau_materials.txt ");
            TableauMaterials.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            if (File.Exists(Common.InputPath + @"\triggers.txt"))
            {
                Print("triggers.txt ");
                Decomp.Triggers.Decompile();
                Print("{0:0} ms\n", sw.ElapsedTicks*1000.0 / Stopwatch.Frequency - dblLastOperation);
                dblLastOperation = sw.ElapsedTicks*1000.0 / Stopwatch.Frequency;
            }
            else
                Print("File \"{0}\\triggers.txt\" not found\n", Common.InputPath);

            Print("troops.txt ");
            Troops.Decompile();
            Print("{0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency - dblLastOperation);
            //dblLastOperation = sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency;

            //MessageBox.Show(Common.InputPath + @"\Data\ground_specs.txt");

            Print("Total time: {0:0} ms\n", sw.ElapsedTicks * 1000.0 / Stopwatch.Frequency);

            if (IsCheckedCheckBox(checkBox1))
                Process.Start(Common.OutputPath);

            SetButtonText(button4, "Decompile");
        }

        private void BrowseSoursePathButtonClick(object sender, RoutedEventArgs e)
        {
            var folderBrowseDialog = new FolderBrowserDialog { Description = @"Choose a file or folder which you want to decompile..." };

            if (Directory.Exists(textBox1.Text))
                folderBrowseDialog.SelectedPath = textBox1.Text;

            if (folderBrowseDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox1.Text = folderBrowseDialog.SelectedPath;
            }
        }

        private void BrowseOutputPathButtonClick(object sender, RoutedEventArgs e)
        {
            var folderBrowseDialog = new FolderBrowserDialog { Description = @"Choose output folder..." };

            if (Directory.Exists(textBox2.Text))
                folderBrowseDialog.SelectedPath = textBox2.Text;

            if (folderBrowseDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox2.Text = folderBrowseDialog.SelectedPath;
            }
        }

        private static string[] InitFactions()
        {
            var fID = new Text(Common.InputPath + @"\factions.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aFactions = new string[n];
            for (int i = 0; i < n; i++)
            {
                string strFacID = fID.GetWord();
                if (strFacID == "0")
                    strFacID = fID.GetWord();
                aFactions[i] = strFacID.Remove(0, 4);

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                for (int r = 0; r < n; r++)
                {
                    fID.GetDouble();
                }
            }
            fID.Close();

            return aFactions;
        }

        private static string[] InitTroops()
        {
            var fID = new Text(Common.InputPath + @"\troops.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aTroops = new string[n];
            for (int i = 0; i < n; i++)
            {
                aTroops[i] = fID.GetWord().Remove(0, 4);

                /*for (int j = 0; j < 9; j++)
                {
                    fID.GetWord();
                }

                for (int j = 0; j < 64; j++)
                {
                    fID.GetWord();
                    fID.GetWord();
                }

                for (int j = 0; j < 26; j++)
                    fID.GetWord();*/
                
                for (int j = 0; j < 163; j++)
                    fID.GetWord();
            }
            fID.Close();

            return aTroops;
        }

        private static string[] InitScenes()
        {
            var fID = new Text(Common.InputPath + @"\scenes.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aScenes = new string[n];
            for (int i = 0; i < n; i++)
            {
                aScenes[i] = fID.GetWord().Remove(0, 4);

                for (int j = 0; j < 10; j++)
                {
                    fID.GetWord();
                }

                int iPassages = fID.GetInt();
                for (int j = 0; j < iPassages; j++)
                {
                    fID.GetWord();
                }

                int iChestTroops = fID.GetInt();
                for (int j = 0; j < iChestTroops; j++)
                {
                    fID.GetWord();
                }

                fID.GetWord();
                //idFile.ReadLine();
                //idFile.ReadLine();
                //idFile.ReadLine();
            }
            fID.Close();

            return aScenes;
        }

        private static string[] InitMapIcons()
        {
            var fID = new Text(Common.InputPath + @"\map_icons.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aMapIcons = new string[n];
            for (int i = 0; i < n; i++)
            {
                aMapIcons[i] = fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                int iTriggers = fID.GetInt();
                for (int t = 0; t < iTriggers; t++)
                {
                    //idFile.GetString();
                    fID.GetWord();

                    int iRecords = fID.GetInt();
                    if (iRecords != 0)
                    {
                        for (int r = 0; r < iRecords; r++)
                        {
                            fID.GetWord();
                            int iParams = fID.GetInt();
                            for (int p = 0; p < iParams; p++)
                            {
                                fID.GetWord();
                            }
                        }
                    }
                }
            }
            fID.Close();

            return aMapIcons;
        }

        private static string[] InitSkills()
        {
            var fID = new StreamReader(Common.InputPath + @"\skills.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aSkills = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aSkills[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 4);
            }
            fID.Close();

            return aSkills;
        }

        private static string[] InitItems()
        {
            var fID = new Text(Common.InputPath + @"\item_kinds1.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aItems = new string[n];
            for (int i = 0; i < n; i++)
            {
                string strID = fID.GetWord();
                aItems[i] = strID.Remove(0, 4);
                fID.GetWord();
                fID.GetWord();

                int iMeshes = fID.GetInt();

                for (int m = 0; m < iMeshes; m++)
                {
                    fID.GetWord();
                    fID.GetWord();
                }

                for (int v = 0; v < 17; v++)
                {
                    fID.GetWord();
                }

                int iFactions = fID.GetInt();
                for (int j = 0; j < iFactions; j++)
                    fID.GetInt();

                int iTriggers = fID.GetInt();
                for (int t = 0; t < iTriggers; t++)
                {
                    fID.GetWord();
                    int iRecords = fID.GetInt();
                    for (int r = 0; r < iRecords; r++)
                    {
                        fID.GetWord();
                        int iParams = fID.GetInt();
                        for (int p = 0; p < iParams; p++)
                        {
                            fID.GetWord();
                        }
                    }
                }
            }
            fID.Close();

            return aItems;
        }

        private static string[] InitParticles()
        {
            var fID = new Text(Common.InputPath + @"\particle_systems.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aParticleSystems = new string[n];
            for (int i = 0; i < n; i++)
            {
                aParticleSystems[i] = fID.GetWord().Remove(0, 5);

                for (int j = 0; j < 37; j++)
                    fID.GetWord();
            }
            fID.Close();

            return aParticleSystems;
        }

        private static string[] InitMenus()
        {
            var fID = new Text(Common.InputPath + @"\menus.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aMenus = new string[n];
            for (int i = 0; i < n; i++)
            {
                string strID = fID.GetWord();
                aMenus[i] = strID.Remove(0, 5);

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                int iRecords = fID.GetInt();
                if (iRecords != 0)
                {
                    for (int r = 0; r < iRecords; r++)
                    {
                        fID.GetWord();
                        int iParams = fID.GetInt();
                        for (int p = 0; p < iParams; p++)
                        {
                            fID.GetWord();
                        }
                    }
                }

                int iMenuOptions = fID.GetInt();
                for (int j = 0; j < iMenuOptions; j++)
                {
                    fID.GetWord();
                    iRecords = fID.GetInt();
                    if (iRecords != 0)
                    {
                        for (int r = 0; r < iRecords; r++)
                        {
                            fID.GetWord();
                            int iParams = fID.GetInt();
                            for (int p = 0; p < iParams; p++)
                            {
                                fID.GetWord();
                            }
                        }
                    }

                    fID.GetWord();

                    iRecords = fID.GetInt();
                    if (iRecords != 0)
                    {
                        for (int r = 0; r < iRecords; r++)
                        {
                            fID.GetWord();
                            int iParams = fID.GetInt();
                            for (int p = 0; p < iParams; p++)
                            {
                                fID.GetWord();
                            }
                        }
                    }

                    fID.GetWord();
                }


                //idFile.ReadLine();
            }
            fID.Close();

            return aMenus;
        }

        private static string[] InitMeshes()
        {
            var fID = new StreamReader(Common.InputPath + @"\meshes.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aMeshes = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aMeshes[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 5);
            }
            fID.Close();

            return aMeshes;
        }

        private static string[] InitMusic()
        {
            var fID = new StreamReader(Common.InputPath + @"\music.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aMusic = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str == null)
                    continue;

                aMusic[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];
                aMusic[i] = aMusic[i].Remove(aMusic[i].Length - 4, 4);
            }
            fID.Close();

            return aMusic;
        }

        private static string[] InitParties()
        {
            var fID = new Text(Common.InputPath + @"\parties.txt");
            fID.GetString();
            int n = fID.GetInt();
            fID.GetInt();

            var idParties = new string[n];
            for (int i = 0; i < n; i++)
            {
                fID.GetWord(); fID.GetWord(); fID.GetWord();
                idParties[i] = fID.GetWord().Remove(0, 2);

                for (int j = 0; j < 17; j++)
                    fID.GetWord();

                int iRecords = fID.GetInt();

                for (int j = 0; j < iRecords; j++)
                {
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                }

                fID.GetWord();
            }
            fID.Close();

            return idParties;
        }

        private static string[] InitPartyTemplates()
        {
            var fID = new StreamReader(Common.InputPath + @"\party_templates.txt", Encoding.ASCII);
            fID.ReadLine();
            int n = Convert.ToInt32(fID.ReadLine());
            var aPartyTemplates = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aPartyTemplates[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 3);
            }
            fID.Close();

            return aPartyTemplates;
        }

        private static string[] InitPresentations()
        {
            var fID = new Text(Common.InputPath + @"\presentations.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aPresentations = new string[n];
            for (int i = 0; i < n; i++)
            {
                aPresentations[i] = fID.GetWord().Remove(0, 6);
                //idPresentations[i - 1] = presentation.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 6);
                //var numEvents = Convert.ToInt32(presentation.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[3]);
                fID.GetWord();
                fID.GetWord();

                var iEvents = fID.GetInt();

                while (iEvents != 0)
                {
                    fID.GetWord();

                    int iRecords = fID.GetInt();
                    if (iRecords != 0)
                    {
                        for (int r = 0; r < iRecords; r++)
                        {
                            fID.GetWord();
                            int iParams = fID.GetInt();
                            for (int p = 0; p < iParams; p++)
                            {
                                fID.GetWord();
                            }
                        }
                    }
                    iEvents--;
                }

                //idFile.ReadLine();
                //idFile.ReadLine();
            }
            fID.Close();

            return aPresentations;
        }

        private static string[] InitQuests()
        {
            var fID = new StreamReader(Common.InputPath + @"\quests.txt", Encoding.ASCII);
            fID.ReadLine();
            int n = Convert.ToInt32(fID.ReadLine());
            var aQuests = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aQuests[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 4);
            }
            fID.Close();

            return aQuests;
        }

        private static string[] InitQuickStrings()
        {
            var fID = new StreamReader(Common.InputPath + @"\quick_strings.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aQuickStrings = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aQuickStrings[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[1].Replace('_', ' ');
            }
            fID.Close();

            return aQuickStrings;
        }

        private static string[] InitSceneProps()
        {
            var fID = new Text(Common.InputPath + @"\scene_props.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aSceneProps = new string[n];
            for (int i = 0; i < n; i++)
            {
                aSceneProps[i] = fID.GetWord().Remove(0, 4);

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                var iTriggers = fID.GetInt();

                //idSceneProps[i - 1] = sceneprop.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 4);
                //var numTriggers = Convert.ToInt32(sceneprop.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[5]);

                while (iTriggers != 0)
                {
                    fID.GetWord();

                    int iRecords = fID.GetInt();
                    if (iRecords != 0)
                    {
                        for (int r = 0; r < iRecords; r++)
                        {
                            fID.GetWord();
                            int iParams = fID.GetInt();
                            for (int p = 0; p < iParams; p++)
                            {
                                fID.GetWord();
                            }
                        }
                    }
                    iTriggers--;
                }

                //idFile.ReadLine();
                //idFile.ReadLine();
            }
            fID.Close();

            return aSceneProps;
        }

        private static string[] InitScripts()
        {
            var fID = new Text(Common.InputPath + @"\scripts.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aScripts = new string[n];
            for (int i = 0; i < n; i++)
            {
                aScripts[i] = fID.GetWord();

                fID.GetWord();

                int iRecords = fID.GetInt();
                if (iRecords != 0)
                {
                    for (int r = 0; r < iRecords; r++)
                    {
                        fID.GetWord();
                        int iParams = fID.GetInt();
                        for (int p = 0; p < iParams; p++)
                        {
                            fID.GetWord();
                        }
                    }
                }
            }
            fID.Close();

            return aScripts;
        }

        private static string[] InitSounds()
        {
            var fID = new Text(Common.InputPath + @"\sounds.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aSounds = new string[n];

            for (int i = 0; i < n; i++)
            {
                fID.GetWord();
                fID.GetWord();
            }

            n = fID.GetInt();
            for (int i = 0; i < n; i++)
            {
                aSounds[i] = fID.GetWord().Remove(0, 4);
                fID.GetWord();
                int iListCount = fID.GetInt();
                for (int l = 0; l < iListCount; l++)
                {
                    fID.GetWord();
                    fID.GetWord();
                }
            }
            fID.Close();

            return aSounds;
        }

        private static string[] InitStrings()
        {
            var fID = new StreamReader(Common.InputPath + @"\strings.txt", Encoding.ASCII);
            fID.ReadLine();
            int n = Convert.ToInt32(fID.ReadLine());
            var aStrings = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aStrings[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 4);
            }
            fID.Close();

            return aStrings;
        }

        private static string[] InitTableauMateriales()
        {
            var fID = new StreamReader(Common.InputPath + @"\tableau_materials.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aTableauMateriales = new string[n];
            for (int i = 0; i < n; i++)
            {
                var str = fID.ReadLine();
                if (str != null)
                    aTableauMateriales[i] = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0].Remove(0, 4);
            }
            fID.Close();

            return aTableauMateriales;
        }

        private static string[] InitVariables()
        {
            int n = File.ReadAllLines(Common.InputPath + @"\variables.txt").Length;
            var aVariables = new string[n];
            var fID = new StreamReader(Common.InputPath + @"\variables.txt", Encoding.ASCII);
            for (int i = 0; i < n; i++)
            {
                aVariables[i] = fID.ReadLine();
            }
            fID.Close();

            return aVariables;
        }

        private static string[] InitDialogStates()
        {
            int n = File.ReadAllLines(Common.InputPath + @"\dialog_states.txt").Length;
            var aDialogStates = new string[n];
            var fID = new StreamReader(Common.InputPath + @"\dialog_states.txt", Encoding.ASCII);
            for (int i = 0; i < n; i++)
            {
                aDialogStates[i] = fID.ReadLine();
            }
            fID.Close();

            return aDialogStates;
        }

        private static string[] InitMissionTemplates()
        {
            var fID = new Text(Common.InputPath + @"\mission_templates.txt");
            fID.GetString();
            int n = Convert.ToInt32(fID.GetString());
            var aMissionTemplates = new string[n];
            for (int i = 0; i < n; i++)
            {
                fID.GetWord();
                aMissionTemplates[i] = fID.GetWord();

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                int iSpawnRecords = fID.GetInt();
                for (int j = 0; j < iSpawnRecords; j++)
                {
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();

                    int iItems = fID.GetInt();

                    for (int k = 0; k < iItems; k++)
                    {
                        //source.Write("{0},", Common.Items[mt.GetInt()]);
                        fID.GetWord();
                    }
                }
                int iTriggers = fID.GetInt();
                for (int t = 0; t < iTriggers; t++)
                {
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();

                    int iRecords = fID.GetInt();
                    for (int r = 0; r < iRecords; r++)
                    {
                        fID.GetWord();
                        int iParams = fID.GetInt();
                        for (int p = 0; p < iParams; p++)
                        {
                            fID.GetWord();
                        }
                    }

                    iRecords = fID.GetInt();
                    for (int r = 0; r < iRecords; r++)
                    {
                        fID.GetWord();
                        int iParams = fID.GetInt();
                        for (int p = 0; p < iParams; p++)
                        {
                            fID.GetWord();
                        }
                    }
                }
            }
            fID.Close();

            return aMissionTemplates;
        }

        private static string[] InitAnimations()
        {
            var fID = new StreamReader(Common.InputPath + @"\actions.txt", Encoding.ASCII);
            int n = Convert.ToInt32(fID.ReadLine());
            var aAnimations = new string[n];
            for (int i = 0; i < n; i++)
            {
                string animation = fID.ReadLine();
                if (animation == null)
                    continue;

                aAnimations[i] = animation.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[0];

                int j = Convert.ToInt32(animation.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)[3]);
                while (j != 0)
                {
                    fID.ReadLine();
                    j--;
                }
            }
            fID.Close();

            return aAnimations;
        }

        private static string[] InitSkins()
        {
            var fID = new Text(Common.InputPath + @"\skins.txt");
            fID.GetString();
            int n = fID.GetInt();
            var aSkins = new string[n];
            for (int i = 0; i < n; i++)
            {
                aSkins[i] = fID.GetWord();

                fID.GetInt();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                int iFaceKeys = fID.GetInt();
                for (int j = 0; j < iFaceKeys; j++)
                {
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                    fID.GetWord();
                }

                int iMeshesHair = fID.GetInt();
                for (int j = 0; j < iMeshesHair; j++)
                {
                    fID.GetWord();
                }

                int iMeshesBeard = fID.GetInt();
                for (int j = 0; j < iMeshesBeard; j++)
                {
                    fID.GetWord();
                }

                for (int j = 0; j < 2; j++)
                {
                    int iTextures = fID.GetInt();
                    for (int t = 0; t < iTextures; t++)
                    {
                        fID.GetWord();
                    }
                }

                int iTexturesFace = fID.GetInt();
                for (int j = 0; j < iTexturesFace; j++)
                {
                    fID.GetWord();
                    fID.GetWord();
                    int iHairMats = fID.GetInt();
                    int iHairColors = fID.GetInt();
                    for (int m = 0; m < iHairMats; m++)
                    {
                        fID.GetWord();
                    }
                    for (int c = 0; c < iHairColors; c++)
                    {
                        fID.GetWord();
                    }
                }

                int iVoices = fID.GetInt();
                for (int v = 0; v < iVoices; v++)
                {
                    fID.GetWord();
                    fID.GetWord();
                }

                fID.GetWord();
                fID.GetWord();
                fID.GetWord();
                fID.GetWord();

                int iConstraints = fID.GetInt();
                for (int j = 0; j < iConstraints; j++)
                {
                    fID.GetWord();
                    fID.GetWord();

                    int count = fID.GetInt();
                    for (int c = 0; c < count; c++)
                    {
                        fID.GetWord();
                        fID.GetWord();
                    }
                }
            }
            fID.Close();

            return aSkins;
        }

        private void AboutMenuClick(object sender, RoutedEventArgs e)
        {
            var hWnd = new AboutWindow();
            hWnd.Show();
        }

        private void WndClosing(object sender, CancelEventArgs e)
        {
            _hWorkThread.Abort();
            SaveUI();
        }

        private void WndLoaded(object sender, RoutedEventArgs e)
        {
            InitUI();
        }
    }
}
