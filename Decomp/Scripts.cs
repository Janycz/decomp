﻿using System.IO;

namespace Decomp
{
    public static class Scripts
    {
        public static void Decompile()
        {
            var fScripts = new Text(Common.InputPath + @"\scripts.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_scripts.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.ScriptsHeader);
            fScripts.GetString();
            int iScripts = fScripts.GetInt();
            
            for (int s = 0; s < iScripts; s++)
            {
                fSource.Write("  (\"{0}\",\r\n  [\r\n", fScripts.GetWord());
                fScripts.GetInt();
                int iRecords = fScripts.GetInt();
                Common.PrintStatement(ref fScripts, ref fSource, iRecords, "    ");
                fSource.Write("  ]),\r\n\r\n");
            }
            fSource.Write("]");
            fSource.Close();
            fScripts.Close();
        }
    }
}
