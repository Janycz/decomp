﻿using System.IO;

namespace Decomp
{
    public static class SimpleTriggers
    {
        public static void Decompile()
        {
            var fTriggers = new Text(Common.InputPath + @"\simple_triggers.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_simple_triggers.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.TriggersHeader);
            fTriggers.GetString();
            int iSimpleTriggers = fTriggers.GetInt();
            for (int t = 0; t < iSimpleTriggers; t++)
            {
                fSource.Write("  ({0},\r\n  [", Common.GetTriggerParam(fTriggers.GetDouble()));
                int iRecords = fTriggers.GetInt();
                if (iRecords != 0)
                {
                    fSource.WriteLine();
                    Common.PrintStatement(ref fTriggers, ref fSource, iRecords, "    ");
                    fSource.Write("  ");
                }
                fSource.Write("]),\r\n\r\n");
            }
            fSource.Write("]");
            fSource.Close();
            fTriggers.Close();
        }
    }
}
