﻿using System.IO;
using DWORD = System.UInt32;

namespace Decomp
{
    public static class SceneProps
    {
        public static string DecompileFlags(DWORD dwFlag)
        {
            string strFlag = "";

            DWORD iHitPoints = (dwFlag >> 20) & 0xFF;
            if (iHitPoints != 0)
                strFlag = "spr_hit_points(" + iHitPoints + ")|";
            DWORD iUseTime = (dwFlag >> 28) & 0xFF;
            if (iUseTime != 0)
                strFlag += "spr_use_time(" + iUseTime + ")|";

            //dwFlag = dwFlag - iHitPoints - iUseTime;

            string[] strFlags = { "sokf_type_container","sokf_type_ai_limiter","sokf_type_barrier","sokf_type_barrier_leave","sokf_type_ladder",
            "sokf_type_barrier3d","sokf_type_player_limiter","sokf_add_fire","sokf_add_smoke","sokf_add_light","sokf_show_hit_point_bar","sokf_place_at_origin",
            "sokf_dynamic","sokf_invisible","sokf_destructible","sokf_moveable","sokf_face_player","sokf_dynamic_physics","sokf_missiles_not_attached",
            "sokf_enforce_shadows","sokf_dont_move_agent_over","sokf_handle_as_flora","sokf_static_movement"};
            DWORD[] dwFlags = { 0x0000000000000005, 0x0000000000000008, 0x0000000000000009, 0x000000000000000a, 0x000000000000000b, 0x000000000000000c, 
            0x000000000000000d, 0x0000000000000100, 0x0000000000000200, 0x0000000000000400, 0x0000000000000800, 0x0000000000001000, 0x0000000000002000, 
            0x0000000000004000, 0x0000000000008000, 0x0000000000010000, 0x0000000000020000, 0x0000000000040000, 0x0000000000080000, 0x0000000000100000, 
            0x0000000000200000, 0x0000000001000000, 0x0000000002000000 };

            for (int i = 0; i < dwFlags.Length; i++)
            {
                DWORD temp = dwFlag & dwFlags[i];
                if ((temp - dwFlags[i]) == 0)
                {
                    dwFlag ^= dwFlags[i];
                    strFlag += strFlags[i] + "|";
                }
            }

            strFlag = strFlag == "" ? "0" : strFlag.Remove(strFlag.Length - 1, 1);

            return strFlag;
        }

        public static void Decompile()
        {
            var fSceneProps = new Text(Common.InputPath + @"\scene_props.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_scene_props.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.ScenePropsHeader);
            fSceneProps.GetString();
            int iSceneProps = fSceneProps.GetInt();

            for (int i = 0; i < iSceneProps; i++)
            {
                string strID = fSceneProps.GetWord();
                DWORD dwFlag = fSceneProps.GetUInt();
                fSceneProps.GetInt();
                fSource.Write("  (\"{0}\", {1}, \"{2}\", \"{3}\", [", strID.Remove(0, 4), DecompileFlags(dwFlag), fSceneProps.GetWord(), fSceneProps.GetWord());
                
		        int iTriggers = fSceneProps.GetInt();

                for (int t = 0; t < iTriggers; t++)
                {
                    double dInterval = fSceneProps.GetDouble();
                    fSource.Write("\r\n    ({0},[\r\n", Common.GetTriggerParam(dInterval));

                    int iRecords = fSceneProps.GetInt();
                    if (iRecords != 0)
                    {
                        Common.PrintStatement(ref fSceneProps, ref fSource, iRecords, "      ");
                    }
                    fSource.WriteLine("    ]),");
                }
                fSource.WriteLine(iTriggers > 0 ? "  ]),\r\n" : "]),\r\n");
            }
            fSource.Write("]");
            fSource.Close();
            fSceneProps.Close();
        }
    }
}
