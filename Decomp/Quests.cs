﻿using System.IO;

namespace Decomp
{
    public static class Quests
    {
        public static string DecompileFlags(int iFlag)
        {
            switch (iFlag)
            {
                case 0x00000001:
                    return "qf_show_progression";
                case 0x00000002:
                    return "qf_random_quest";
                case 0x00000003:
                    return "qf_show_progression|qf_random_quest";
                default:
                    return "0";
            }
        }

        public static void Decompile()
        {
            var fQuests = new Text(Common.InputPath + @"\quests.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_quests.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.QuestsHeader);
            fQuests.GetString();
            int iQuests = fQuests.GetInt();

            for (int iQuest = 0; iQuest < iQuests; iQuest++)
                fSource.WriteLine("  (\"{0}\", \"{1}\", {2}, \"{3}\"),", fQuests.GetWord().Remove(0, 4), fQuests.GetWord().Replace('_', ' '), DecompileFlags(fQuests.GetInt()), fQuests.GetWord().Replace('_', ' '));

            fSource.Write("]");
            fSource.Close();
            fQuests.Close();
        }
    }
}
