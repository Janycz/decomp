﻿using System;
using System.IO;
using DWORD = System.UInt32;

namespace Decomp
{
    public static class Music
    {
        public static string DecompileFlags(DWORD dwFlag)
        {
            string strFlag = "";
            string[] strMusicFlags = { "mtf_looping", "mtf_start_immediately", "mtf_persist_until_finished", "mtf_sit_tavern", "mtf_sit_fight", "mtf_sit_multiplayer_fight",
			"mtf_sit_ambushed", "mtf_sit_town", "mtf_sit_town_infiltrate", "mtf_sit_killed", "mtf_sit_travel", "mtf_sit_arena", "mtf_sit_siege", "mtf_sit_night",
			"mtf_sit_day", "mtf_sit_encounter_hostile", "mtf_sit_main_title", "mtf_sit_victorious", "mtf_sit_feast", "mtf_module_track" };
		    DWORD[] dwMusicFlags = { 0x00000040, 0x00000080, 0x00000100, 0x00000200, 0x00000400, 0x00000800, 0x00001000, 0x00002000, 0x00004000, 0x00008000, 0x00010000,
			0x00020000, 0x00040000, 0x00080000, 0x00100000, 0x00200000, 0x00400000, 0x00800000, 0x01000000, 0x10000000 };
            DWORD dwCulture = (dwFlag & 0x3F);
            
            if (dwCulture == 0x3F)
            {
                strFlag = "mtf_culture_all";
                dwFlag ^= 0x3F;
            }
            else if (dwCulture != 0)
            {
                for (uint t = 1, i = 1; t <= 0x20; t *= 2, i++)
                {
                    if ((dwCulture & t) != 0)
                    {
                        strFlag += String.Format("{0}mtf_culture_{1}", strFlag != "" ? "|" : "", i);
                        dwFlag ^= t;
                    }
                }
            }

            for (int i = 0; i < dwMusicFlags.Length; i++)
            {
                if ((dwFlag & dwMusicFlags[i]) != 0)
                {
                    if (strFlag != "")
                        strFlag = strFlag + "|";
                    strFlag += strMusicFlags[i];
                    dwFlag ^= dwMusicFlags[i];
                }
            }

            if (strFlag == "") strFlag = "0";

            return strFlag;
        }

        public static void Decompile()
        {
            var fMusic = new Text(Common.InputPath + @"\music.txt");
            var fSource = new StreamWriter(Common.OutputPath + @"\module_music.py");
            fSource.WriteLine(Header.StandardHeader);
            fSource.WriteLine(Header.MusicHeader);
            int iTracks = fMusic.GetInt();
            for (int t = 0; t < iTracks; t++)
            {
                string strTrack = fMusic.GetWord();
                DWORD dwTrackFlags = fMusic.GetUInt();
                DWORD dwContinueFlags = fMusic.GetUInt();
                fSource.WriteLine("  (\"{0}\", \"{1}\", {2}, {3}),", strTrack.Remove(strTrack.Length - 4, 4), strTrack, DecompileFlags(dwTrackFlags), DecompileFlags(dwContinueFlags ^ dwTrackFlags));
            }
            fSource.Write("]");
            fSource.Close();
            fMusic.Close();
        }
    }
}
