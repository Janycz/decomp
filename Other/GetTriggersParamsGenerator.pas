program GetTriggersParamsGenerator;
var 
  s:string;
  f:Text;
  splitter:char;
  str1:string;
  str2:string;
begin
  str1 := '';
  str2 := '';
  splitter := '=';
  Assign(f,'header_triggers.py');
  Reset(f);
  while not(Eof(f)) do
  begin
    readln(f,s);
    if s.Contains('=')
    then
      begin
        var SArray := s.Split(splitter);
        //str1 := SArray[0];
        //str2 := str2 + SArray[1] + ',';
        writeln('case ',SArray[1].Replace('.0', ''), ': return "', SArray[0], '";'); 
      end;  
  end;
  //writeln(str1,#10#13,str2);
  Close(f);
end.  
