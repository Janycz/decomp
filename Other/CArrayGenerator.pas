program CArrayGenerator;
var 
  s:string;
  f:Text;
  splitter:char;
begin
  splitter := '=';
  Assign(f,'header_operations.py');
  Reset(f);
  while not(Eof(f)) do
  begin
    readln(f,s);
    if s.Contains('=')
    then
      begin
        var SArray:=s.Split(splitter);
        writeln('new Command("' + SArray[0] + '",' + SArray[1] + '),');
      end;  
  end;
  Close(f);
end.  
