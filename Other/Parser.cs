﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser
{
    static internal class Program
    {
        static void Main(string[] args)
        {
            //string strMsg = @"
            //        else if ((iOpCode == {0}) && p == {1} && {2})
            //        {
            //            strParam = DecompileTextFlags(Convert.ToUInt32(strParam));
            //        }";
            var fInput = new StreamReader("header_operations.py");
            var fOutput = new StreamWriter("header_operations_parsed.py");

            while (!fInput.EndOfStream)
            {
                string strCommand = fInput.ReadLine();
                if (strCommand == null) continue;
                if (strCommand.Contains("="))
                {
                    try
                    {
                        int iCodePosBegin = strCommand.IndexOf('=');
                        int iCodePosEnd = strCommand.IndexOf('#');
                        int iOpCode = Convert.ToInt32(strCommand.Substring(iCodePosBegin, iCodePosEnd - iCodePosBegin).Replace("#", "").Replace("=", ""));

                        string strOp = strCommand.Substring(0, iCodePosBegin).Replace("=", "").Trim();

                        int iPosBegin = strCommand.IndexOf('(');
                        int iPosEnd = strCommand.IndexOf(')');
                        //fOutput.WriteLine(iOpCode + "   " + strCommand.Substring(iPosBegin + 1, iPosEnd - iPosBegin - 1));
                        
                        string strParams = strCommand.Substring(iPosBegin + 1, iPosEnd - iPosBegin - 1);
                        string[] aParams = strParams.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);
                        Console.WriteLine("Parse {0} Operand", iOpCode);
                        for (int i = 1; i < aParams.Length; i++)
                        {
                            string strParam = aParams[i];
                            string strCond = GetCondition(strParam.Replace("<", "").Replace(">", "").Trim());
                            if(strCond == null) continue;
                            try
                            {
                                fOutput.Write("else if ((iOpCode == {0}) && p == {1} && {2}) //{3}", iOpCode, i - 1, strCond, strOp);
                                fOutput.WriteLine("\r\n{\r\n    " + GetFunction(strParam.Replace("<", "").Replace(">", "").Trim()) + "\r\n}");
                            }
                            catch
                            {
                                Console.WriteLine("ERROR");
                            }
                        }
                    }catch{}
                }
            }
            fInput.Close();

            var fInputWSE = new StreamReader("header_operations_addon.py");
            while (!fInputWSE.EndOfStream)
            {
                string strCommand = fInputWSE.ReadLine();
                if (strCommand == null) continue;
                if (strCommand.Contains("="))
                {
                    try
                    {
                        int iCodePosBegin = strCommand.IndexOf('=');
                        int iCodePosEnd = strCommand.IndexOf('#');
                        int iOpCode = Convert.ToInt32(strCommand.Substring(iCodePosBegin, iCodePosEnd - iCodePosBegin).Replace("#", "").Replace("=", ""));

                        string strOp = strCommand.Substring(0, iCodePosBegin).Replace("=", "").Trim();

                        int iPosBegin = strCommand.IndexOf('(');
                        int iPosEnd = strCommand.IndexOf(')');
                        //fOutput.WriteLine(iOpCode + "   " + strCommand.Substring(iPosBegin + 1, iPosEnd - iPosBegin - 1));

                        string strParams = strCommand.Substring(iPosBegin + 1, iPosEnd - iPosBegin - 1);
                        string[] aParams = strParams.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        Console.WriteLine("Parse {0} Operand", iOpCode);
                        for (int i = 1; i < aParams.Length; i++)
                        {
                            string strParam = aParams[i];
                            string strCond = GetCondition(strParam.Replace("<", "").Replace(">", "").Trim());
                            if (strCond == null) continue;
                            try
                            {
                                fOutput.Write("else if ((iOpCode == {0}) && p == {1} && {2}) //{3}", iOpCode, i - 1, strCond, strOp);
                                fOutput.WriteLine("\r\n{\r\n    " + GetFunction(strParam.Replace("<", "").Replace(">", "").Trim()) + "\r\n}");
                            }
                            catch
                            {
                                Console.WriteLine("ERROR");
                            }
                        }
                    }
                    catch { }
                }
            }
            fInputWSE.Close();
            fOutput.Close();

            //Console.ReadKey();
        }

        public static string GetCondition(string strParam)
        {
            string[] strParams = { "face_key_register", "fp_register", "game_key_code", "key_code", "position", "string", "game_key_no" };
            string[] strCond = { "IsFaceKey(Convert.ToUInt64(strParam))", "IsFloatRegister(Convert.ToUInt64(strParam))", "IsKey(Convert.ToUInt64(strParam))", 
                                 "IsKey(Convert.ToUInt64(strParam))", "IsPosition(Convert.ToUInt64(strParam))", "IsStringRegister(Convert.ToUInt64(strParam))", 
                                 "IsKey(Convert.ToUInt64(strParam))" };
            for (int i = 0; i < strParams.Length; i++)
            {
                if (strParam.Contains(strParams[i]))
                {
                    return strCond[i];
                }
            }
            return null;
        }

        public static string GetFunction(string strParam)
        {
            string[] strParams = { "face_key_register", "fp_register", "game_key_code", "key_code", "position", "string", "game_key_no" };
            string[] strFunctions = { "strParam = GetFaceKey(Convert.ToUInt64(strParam));", "strParam =  = \"fp\" + strParam;", 
                                        "strParam = GetGameKey(Convert.ToUInt64(strParam));", "strParam = GetKey(Convert.ToUInt64(strParam));", 
                                        "strParam = \"pos\" + strParam;", "strParam = \"s\" + strParam;", "strParam = GetGameKey(Convert.ToUInt64(strParam));" };
            for (int i = 0; i < strParams.Length; i++)
            {
                if (strParam.Contains(strParams[i]))
                {
                    return strFunctions[i];
                }
            }
            return null;
        }
    }
}
